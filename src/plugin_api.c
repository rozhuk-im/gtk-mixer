/*-
 * Copyright (c) 2020 - 2021 Rozhuk Ivan <rozhuk.im@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Rozhuk Ivan <rozhuk.im@gmail.com>
 *
 */


#include <sys/param.h>
#include <sys/types.h>
#include <inttypes.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include "plugin_api.h"


int
gm_plugin_init(gm_plugin_p *plugins, size_t *plugins_count,
    gmp_dev_list_p dev_list) {
	gm_plugin_p plugin;

	(*plugins_count) = nitems(plugins_descr);
	(*plugins) = calloc(nitems(plugins_descr), sizeof(gm_plugin_t));

	for (size_t i = 0, j = 0; i < nitems(plugins_descr); i ++) {
		plugin = plugins[j];
		plugin->descr = plugins_descr[i];
		if (NULL != plugin->descr->plugin_init) {
			if (0 != plugin->descr->plugin_init(plugin))
				continue;
		}
		plugin->descr->plugin_list_devs(plugin, dev_list);
		j ++;
	}

	return (0);
}

int
gm_plugin_dev_list_add(gm_plugin_p plugin, gmp_dev_list_p dev_list,
    gmp_dev_p dev) {
	gmp_dev_t gdd = (*dev);
	gmp_dev_p dev_new;

	if (NULL == plugin || NULL == dev_list || NULL == dev)
		return (EINVAL);

	if (NULL == gdd.name || 0 == gdd.name[0]) {
		gdd.name = plugin->descr->name;
	}
	if (NULL == gdd.description || 0 == gdd.description[0]) {
		gdd.description = "Default";
	}

	dev_new = reallocarray(dev_list->devs,
	    (dev_list->count + 2),
	    sizeof(gmp_dev_t));
	if (NULL == dev_new)
		return (ENOMEM);
	dev_list->devs = dev_new;
	memset(&dev_list->devs[dev_list->count], 0x00, sizeof(gmp_dev_t));
	dev_list->devs[dev_list->count].name = strdup(gdd.name);
	dev_list->devs[dev_list->count].description = strdup(gdd.description);
	dev_list->devs[dev_list->count].plugin = plugin;
	dev_list->count ++;

	return (0);
}


int
gm_plugin_dev_init(gmp_dev_p dev) {
	int error;

	if (NULL == dev)
		return (EINVAL);

	if (NULL != dev->plugin->descr->dev_init) {
		error = dev->plugin->descr->dev_init(dev);
		if (0 != error)
			return (error);
	}

	return (gm_plugin_dev_read(dev, 1));
}

void
gm_plugin_dev_uninit(gmp_dev_p dev) {
	gmp_dev_line_p dev_line;

	if (NULL == dev)
		return;

	if (NULL != dev->plugin->descr->dev_uninit) {
		dev->plugin->descr->dev_uninit(dev);
	}

	if (NULL != dev->lines && 0 != dev->lines_count) {
		for (size_t i = 0; i < dev->lines_count; i ++) {
			dev_line = &dev->lines[i];
			free((void*)dev_line->display_name);
		}
		free(dev->lines);
		dev->lines = NULL;
		dev->lines_count = 0;
	}
}

int
gm_plugin_dev_read(gmp_dev_p dev, int force) {
	int error;
	gmp_dev_line_p dev_line;
	gmp_dev_line_state_t state_muted, state;

	if (NULL == dev)
		return (EINVAL);

	memset(&state_muted, 0x00, sizeof(state_muted));
	for (size_t i = 0; i < dev->lines_count; i ++) {
		dev_line = &dev->lines[i];
		if (0 == force && 0 == dev_line->read_required)
			continue;
		/* Prepare to read. */
		memset(&state, 0x00, sizeof(state));
		/* Read. */
		error = dev->plugin->descr->dev_line_read(dev, dev_line,
		    &state);
		/* Handle errors. */
		if (0 != error)
			return (error);
		dev_line->read_required = 0;
		if (0 != dev_line->has_enable) {
			if (0 != memcmp(&state, &dev_line->state,
			    sizeof(state))) {
				/* Mark as updated. */
				dev_line->is_updated ++;
			}
		} else if (dev_line->state.is_enabled) {
			if (0 != memcmp(state.chan_vol,
			    dev_line->state.chan_vol,
			    sizeof(state.chan_vol))) {
				dev_line->is_updated ++;
			}
		} else {
			/* On mute all channels volumes must be 0. */
			if (0 != memcmp(state_muted.chan_vol,
			    state.chan_vol,
			    sizeof((state_muted.chan_vol)))) {
				/* Mark as unmuted + updated. */
				state.is_enabled = 1;
				dev_line->is_updated ++;
			}		
		}
		if (0 == dev_line->is_updated)
			continue;
		memcpy(&dev_line->state, &state, sizeof(state));
	}

	return (0);
}

int
gm_plugin_dev_write(gmp_dev_p dev, int force) {
	int error;
	gmp_dev_line_p dev_line;
	gmp_dev_line_state_t state_muted;

	if (NULL == dev)
		return (EINVAL);

	memset(&state_muted, 0x00, sizeof(state_muted));
	for (size_t i = 0; i < dev->lines_count; i ++) {
		dev_line = &dev->lines[i];
		if (0 == force && 0 == dev_line->write_required)
			continue;
		/* Write. */
		if (0 == dev_line->state.is_enabled &&
		    0 == dev_line->has_enable) {
			/* Set volumes to zero to simulate line disable. */
			error = dev->plugin->descr->dev_line_write(dev,
			    dev_line, &state_muted);
		} else { /* Set actual levels. */
			error = dev->plugin->descr->dev_line_write(dev,
			    dev_line, &dev_line->state);
		}
		/* Handle errors. */
		if (0 != error)
			return (error);
		dev_line->write_required = 0;
	}

	return (0);
}


int
gm_plugin_dev_line_add(gmp_dev_p dev, const char *display_name,
    gmp_dev_line_p *dev_line_ret) {
	char *dn;
	gmp_dev_line_p dev_line, dev_line_new;

	if (NULL == dev || NULL == display_name)
		return (EINVAL);
	dev_line_new = reallocarray(dev->lines,
	    (dev->lines_count + 2),
	    sizeof(gmp_dev_line_t));
	if (NULL == dev_line_new)
		return (ENOMEM);
	dev->lines = dev_line_new;
	dev_line = &dev->lines[dev->lines_count];
	dev->lines_count ++;

	memset(dev_line, 0x00, sizeof(gmp_dev_line_t));
	/* Remove spaces from end. */
	dn = strdup(display_name);
	for (size_t i = strlen(dn); 0 < i; i --) {
		if (' ' != dn[(i - 1)])
			break;
		dn[(i - 1)] = 0x00;
	}
	dev_line->display_name = dn;

	if (NULL != dev_line_ret) {
		(*dev_line_ret) = dev_line;
	}

	return (0);
}

int
gm_plugin_dev_line_vol_max_get(gmp_dev_line_p dev_line) {
	int ret = 0;

	if (NULL == dev_line)
		return (0);
	for (size_t i = 0; i < MIXER_CHANNELS_COUNT; i ++) {
		if (0 == (((((uint32_t)1) << i) & dev_line->chan_map)))
			continue;
		if (ret < dev_line->state.chan_vol[i]) {
			ret = dev_line->state.chan_vol[i];
		}
	}

	return (ret);
}

void
gm_plugin_dev_line_vol_glob_set(gmp_dev_line_p dev_line, int vol_new) {

	if (NULL == dev_line)
		return;
	for (size_t i = 0; i < MIXER_CHANNELS_COUNT; i ++) {
		if (0 == (((((uint32_t)1) << i) & dev_line->chan_map)))
			continue;
		dev_line->state.chan_vol[i] = vol_new;
	}
}

size_t
gm_plugin_dev_line_chan_first(gmp_dev_line_p dev_line) {

	if (NULL == dev_line)
		return (MIXER_CHANNELS_COUNT);
	for (size_t i = 0; i < MIXER_CHANNELS_COUNT; i ++) {
		if (0 == (((((uint32_t)1) << i) & dev_line->chan_map)))
			continue;
		return (i);
	}

	return (MIXER_CHANNELS_COUNT);
}
size_t
gm_plugin_dev_line_chan_next(gmp_dev_line_p dev_line, size_t cur) {

	if (NULL == dev_line)
		return (MIXER_CHANNELS_COUNT);
	for (size_t i = (cur + 1); i < MIXER_CHANNELS_COUNT; i ++) {
		if (0 == (((((uint32_t)1) << i) & dev_line->chan_map)))
			continue;
		return (i);
	}

	return (MIXER_CHANNELS_COUNT);
}
