/*-
 * Copyright (c) 2020 - 2021 Rozhuk Ivan <rozhuk.im@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Rozhuk Ivan <rozhuk.im@gmail.com>
 *
 */


#ifndef __PLUGIN_API_H__
#define __PLUGIN_API_H__

#include <sys/param.h>
#include <sys/types.h>
#include <inttypes.h>


typedef struct gtk_mixer_plugin_s *gm_plugin_p;
typedef struct gtk_mixer_plugin_device_list_s *gmp_dev_list_p;
typedef struct gtk_mixer_plugin_device_s *gmp_dev_p;
typedef struct gtk_mixer_plugin_device_line_s *gmp_dev_line_p;
typedef struct gtk_mixer_plugin_device_line_state_s *gmp_dev_line_state_p;


/* Discribe plugin API. */
typedef struct gtk_mixer_plugin_description_s {
	const char *name;
	const char *description;
	/* Plugin level functions. */
	int (*plugin_init)(gm_plugin_p plugin); /* Optional. 0 = Ok. */
	void (*plugin_uninit)(gm_plugin_p plugin); /* Optional. */
	void (*plugin_list_devs)(gm_plugin_p plugin, gmp_dev_list_p devs);
	/* Plugin device level functions. */
	int (*dev_init)(gmp_dev_p dev); /* Lines must be added on init. */
	void (*dev_uninit)(gmp_dev_p dev); /* Optional. */
	int (*dev_line_read)(gmp_dev_p dev, gmp_dev_line_p dev_line,
	    gmp_dev_line_state_p state); /* Read from mixer dev to app. */
	int (*dev_line_write)(gmp_dev_p dev, gmp_dev_line_p dev_line,
	    gmp_dev_line_state_p state); /* Write from app to mixer dev. */
	void (*dev_write)(gmp_dev_p dev); /* Write from app to mixer dev. */
} gmp_descr_t, *gmp_descr_p;


typedef struct gtk_mixer_plugin_s {
	gmp_descr_p	descr;
	void 		*priv; /* Plugin internal. */
} gm_plugin_t, *gm_plugin_p;



/* From: https://en.wikipedia.org/wiki/Surround_sound */
#define MIXER_CHANNELS_COUNT	18
enum {
	MIXER_CHANNEL_FL = 0,
	MIXER_CHANNEL_FR,
	MIXER_CHANNEL_FC,
	MIXER_CHANNEL_LFE,
	MIXER_CHANNEL_BL,
	MIXER_CHANNEL_BR,
	MIXER_CHANNEL_FLC,
	MIXER_CHANNEL_FRC,
	MIXER_CHANNEL_BC,
	MIXER_CHANNEL_SL,
	MIXER_CHANNEL_SR,
	MIXER_CHANNEL_TC,
	MIXER_CHANNEL_TFL,
	MIXER_CHANNEL_TFC,
	MIXER_CHANNEL_TFR,
	MIXER_CHANNEL_TBL,
	MIXER_CHANNEL_TBC,
	MIXER_CHANNEL_TBR
};

static const char *channel_name_long[MIXER_CHANNELS_COUNT] = {
	"Front Left",
	"Front Right",
	"Front Center",
	"Low Frequency",
	"Back Left",
	"Back Right",
	"Front Left of Center",
	"Front Right of Center",
	"Back Center",
	"Side Left",
	"Side Right",
	"Top Center",
	"Front Left Height",
	"Front Center Height",
	"Front Right Height",
	"Rear Left Height",
	"Rear Center Height",
	"Rear Right Height",
};

static const char *channel_name_short[MIXER_CHANNELS_COUNT] = {
	"FL",
	"FR",
	"FC",
	"LFE",
	"BL",
	"BR",
	"FLC",
	"FRC",
	"BC",
	"SL",
	"SR",
	"TC",
	"TFL",
	"TFC",
	"TFR",
	"TBL",
	"TBC"
	"TBR"
};


/* Soundcard line: Main, pcm, cd, video... */

/* This is for read/write. */
#define GMPDL_CHAN_VOL_INVALID	0xffffff
typedef struct gtk_mixer_plugin_device_line_state_s {
	int chan_vol[MIXER_CHANNELS_COUNT]; /* Volume level per channel: 0-100. */
	int is_enabled; /* 0 - is line muted / record disabled. */
} gmp_dev_line_state_t, *gmp_dev_line_state_p;

/* Keep all soundcard line data. */
typedef struct gtk_mixer_plugin_device_line_s {
	/* Set by plugin. */
	const char *display_name; /* Line name to display: main, pcm, mic... */
	void *priv; /* Plugin internal. */
	uint32_t chan_map; /* Bitmask for avail channels. */
	size_t chan_vol_count; /* Actual channels count. popcnt(chan_map) */
	int is_capture; /* Device is capture else playback. */
	int is_read_only; /* Only display values, no set. */
	int has_enable; /* Line can be enabled/disabled. (muted) */

	/* Used by app. */
	gmp_dev_line_state_t state;
	int is_updated; /* State changed, need GUI update. */
	int read_required; /* Plugin must read state from mixer. */
	int write_required; /* Plugin must write state to mixer. */
} gmp_dev_line_t, *gmp_dev_line_p;

/* Soundcard. */
typedef struct gtk_mixer_plugin_device_s {
	const char *name; /* Symbolic name. Make it unique! */
	const char *description; /* Verbose human readable name. */
	gm_plugin_p plugin;
	void *priv; /* Plugin internal per device. */
	gmp_dev_line_p lines; /* Auto destroy on dev_uninit. */
	size_t lines_count;
} gmp_dev_t, *gmp_dev_p;

typedef struct gtk_mixer_plugin_device_list_s {
	gmp_dev_p devs;
	size_t count;
} gmp_dev_list_t, *gmp_dev_list_p;



int gm_plugin_init(gm_plugin_p *plugins, size_t *plugins_count,
    gmp_dev_list_p dev_list);

int gm_plugin_dev_list_add(gm_plugin_p plugin, gmp_dev_list_p dev_list,
    gmp_dev_p dev);

int gm_plugin_dev_init(gmp_dev_p dev);
void gm_plugin_dev_uninit(gmp_dev_p dev);

/* Read from mixer dev. */
int gm_plugin_dev_read(gmp_dev_p dev, int force);
/* Write to mixer dev new values. */
int gm_plugin_dev_write(gmp_dev_p dev, int force);

/* Add line to device. */
int gm_plugin_dev_line_add(gmp_dev_p dev, const char *display_name,
    gmp_dev_line_p *dev_line_ret);

/* Get max channel volume per line. */
int gm_plugin_dev_line_vol_max_get(gmp_dev_line_p dev_line);
/* Set volume to all channels on line. */
void gm_plugin_dev_line_vol_glob_set(gmp_dev_line_p dev_line, int vol_new);
/* Get first/next channel index on line. */
size_t gm_plugin_dev_line_chan_first(gmp_dev_line_p dev_line);
size_t gm_plugin_dev_line_chan_next(gmp_dev_line_p dev_line, size_t cur);


#define HAVE_OSS 1

#if HAVE_OSS
extern const gmp_descr_t plugin_oss3;
#endif

static const gmp_descr_t * const plugins_descr[] = {
#if HAVE_OSS
	&plugin_oss3,
#endif
};


#endif /* __PLUGIN_API_H__ */
