/*-
 * Copyright (c) 2020 - 2021 Rozhuk Ivan <rozhuk.im@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Rozhuk Ivan <rozhuk.im@gmail.com>
 *
 */


#ifndef __GTK_MIXER_H__
#define __GTK_MIXER_H__

#include <sys/param.h>
#include <sys/types.h>
#include <inttypes.h>
#include <stdio.h> /* snprintf, fprintf */
#include <time.h>
#include <string.h> /* bcopy, bzero, memcpy, memmove, memset, strerror... */
#include <stdlib.h> /* malloc, exit */
#include <unistd.h> /* close, write, sysconf */

#define GETTEXT_PACKAGE "gtk-mixer"
#include <gtk/gtk.h>
#include <glib/gi18n-lib.h>

#include "plugin_api.h"



typedef struct gtk_mixer_app_s {
	gm_plugin_p	plugins;
	size_t		plugins_count;
	gmp_dev_list_t	dev_list;
	gmp_dev_p	dev_current;
	GtkWidget	*window;
} gm_app_t, *gm_app_p;


const char *volume_stock_from_level(const int is_mic, const int is_enabled,
    const int level, const char *cur_icon_name);

GtkWidget *gtk_mixer_window_create(gm_app_p app);

GtkWidget *gtk_mixer_devs_combo_create(gm_app_p app,
    gmp_dev_p dev);
gmp_dev_p gtk_mixer_devs_combo_get_active_device(GtkWidget *combo);
void gtk_mixer_devs_combo_set_active_device(GtkWidget *combo,
    gmp_dev_p dev);

GtkWidget *gtk_mixer_container_create(void);
void gtk_mixer_container_update_contents(GtkWidget *container,
    gmp_dev_p dev);

GtkWidget *gtk_mixer_line_create(gmp_dev_p dev, gmp_dev_line_p dev_line);



#endif /* __GTK_MIXER_H__ */
