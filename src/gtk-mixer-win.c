/*-
 * Copyright (c) 2008 Jannis Pohlmann <jannis@xfce.org>
 * Copyright (c) 2012 Guido Berhoerster <guido+xfce@berhoerster.name>
 * Copyright (c) 2020 - 2021 Rozhuk Ivan <rozhuk.im@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */


#include <sys/param.h>
#include <sys/types.h>
#include <inttypes.h>

#include "gtk-mixer.h"


typedef struct gtk_mixer_window_s {
	GtkWidget *window;
	gm_app_p app;
	//XfceMixerPreferences *preferences;

	/* Current window state. */
	uint32_t width;
	uint32_t height;
	gboolean is_maximized;
	gboolean is_fullscreen;

	GtkWidget *soundcard_combo;

	/* Active mixer control set. */
	GtkWidget *mixer_frame;
	GtkWidget *mixer_container;

	GtkWidget *select_controls_button;

	GtkWidget *controls_dialog;
} gm_window_t, *gm_window_p;



static void
gtk_mixer_window_update_contents(gm_window_p gm_win) {
	char title[256];
	gmp_dev_p device;

	device = gtk_mixer_devs_combo_get_active_device(gm_win->soundcard_combo);
	if (NULL != device) {
		snprintf(title, sizeof(title),
		    "%s - %s", _("Audio Mixer"),
		    device->description);
		gtk_window_set_title(GTK_WINDOW(gm_win->window), title);
	} else {
		gtk_window_set_title(GTK_WINDOW(gm_win->window), _("Audio Mixer"));
	}
	/* Update the XfceMixerContainer containing the controls */
	gtk_mixer_container_update_contents(gm_win->mixer_container,
	    device);
	/* Make the "Select Controls..." button sensitive */
	gtk_widget_set_sensitive(gm_win->select_controls_button,
	    (NULL != device));
}

static void
gtk_mixer_window_soundcard_changed(GtkWidget *combo __unused,
    gpointer user_data) {
	gm_window_p gm_win = user_data;

	/* Remember the card for next time */
	/*g_signal_handlers_block_by_func(G_OBJECT(gm_win->preferences),
	    gtk_mixer_window_soundcard_property_changed, window);
	g_object_set(G_OBJECT(gm_win->preferences), "sound-card",
	    gtk_mixer_get_card_internal_name(card), NULL);
	g_signal_handlers_unblock_by_func(G_OBJECT(gm_win->preferences),
	    gtk_mixer_window_soundcard_property_changed, window);*/

	/* Update mixer controls for the active sound card */
	gtk_mixer_window_update_contents(gm_win);

	/* Update the controls dialog */
	//if (gm_win->controls_dialog != NULL)
	//	gtk_mixer_controls_dialog_set_soundcard(
	//	    XFCE_MIXER_CONTROLS_DIALOG(gm_win->controls_dialog), device);
}

static void
gtk_mixer_window_quit(GtkButton *button __unused, gpointer user_data) {
	gm_window_p gm_win = user_data;

	gtk_widget_destroy(GTK_WIDGET(gm_win->window));
	g_application_quit(g_application_get_default());
}

static void
gtk_mixer_window_action_select_controls(GtkButton *button __unused,
    gpointer user_data) {
	gm_window_p gm_win = user_data;

#if 0
	g_return_if_fail(gm_win->controls_dialog == NULL);

	gm_win->controls_dialog = gtk_mixer_controls_dialog_new(window);

	gtk_dialog_run(GTK_DIALOG(gm_win->controls_dialog));
	gtk_widget_destroy(gm_win->controls_dialog);
	gm_win->controls_dialog = NULL;
#endif
}

static void
gtk_mixer_window_destroy(GtkWidget *window __unused, gpointer user_data) {
	gm_window_p gm_win = user_data;

	//g_object_set(G_OBJECT(gm_win->preferences), "window-width",
	//    gm_win->current_width, "window-height", gm_win->current_height,
	//    NULL);

	free(gm_win);
}

GtkWidget *
gtk_mixer_window_create(gm_app_p app) {
	gm_window_p gm_win;
	GApplication *application = g_application_get_default();
	GtkWidget *label, *button, *vbox, *hbox, *bbox;
	gmp_dev_p device = NULL;
	gchar *card_name = NULL;
	const gchar *select_controls_accels[] = { "<Control>s", NULL };
	const gchar *quit_accels[] = { "<Control>q", NULL };


	gm_win = calloc(1, sizeof(gm_window_t));
	gm_win->app = app;
	gm_win->window = gtk_dialog_new();

	gm_win->height = 400;
	gm_win->width = 500;
#if 0
	gm_win->preferences = gtk_mixer_preferences_get();

	g_object_get(gm_win->preferences, "window-width",
	    &gm_win->width, "window-height", &gm_win->height,
	    "sound-card", &card_name, NULL);

	if (card_name != NULL) {
		device = gtk_mixer_get_card(card_name);
	} else {
		device = gtk_mixer_get_default_card();
		g_object_set(gm_win->preferences, "sound-card",
		    gtk_mixer_get_card_internal_name(device), NULL);
	}
#endif
	g_free(card_name);

	/* Configure the main window */
	gtk_window_set_type_hint(
	    GTK_WINDOW(gm_win->window), GDK_WINDOW_TYPE_HINT_NORMAL);
	gtk_window_set_icon_name(GTK_WINDOW(gm_win->window),
	    "multimedia-volume-control");
	gtk_window_set_title(GTK_WINDOW(gm_win->window), _("Audio Mixer"));
	gtk_window_set_default_size(GTK_WINDOW(gm_win->window),
	    gm_win->width, gm_win->height);
	gtk_window_set_position(GTK_WINDOW(gm_win->window), GTK_WIN_POS_CENTER);
	//xfce_titled_dialog_set_subtitle(XFCE_TITLED_DIALOG(gm_win->window),
	//    _("Configure sound card(s) and control the volume of selected tracks"));
	g_signal_connect(gm_win->window, "destroy",
	    G_CALLBACK(gtk_mixer_window_destroy), gm_win);

	/* Install action accelerators for the mixer window */
	gtk_application_set_accels_for_action(GTK_APPLICATION(application),
	    "app.select-controls", select_controls_accels);
	gtk_application_set_accels_for_action(
	    GTK_APPLICATION(application), "app.quit", quit_accels);

	vbox = gtk_dialog_get_content_area(GTK_DIALOG(gm_win->window));
	gtk_widget_show(vbox);

	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 12);
	gtk_container_set_border_width(GTK_CONTAINER(hbox), 6);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, TRUE, 0);
	gtk_widget_show(hbox);

	label = gtk_label_new_with_mnemonic(_("Sound _card:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
	gtk_widget_show(label);

	gm_win->soundcard_combo = gtk_mixer_devs_combo_create(app, device);
	g_signal_connect(gm_win->soundcard_combo, "changed",
	    G_CALLBACK(gtk_mixer_window_soundcard_changed), gm_win);
	gtk_box_pack_start(
	    GTK_BOX(hbox), gm_win->soundcard_combo, TRUE, TRUE, 0);
	gtk_label_set_mnemonic_widget(
	    GTK_LABEL(label), gm_win->soundcard_combo);
	gtk_widget_show(gm_win->soundcard_combo);

	gm_win->mixer_frame = gtk_frame_new(NULL);
	gtk_frame_set_shadow_type(
	    GTK_FRAME(gm_win->mixer_frame), GTK_SHADOW_NONE);
	gtk_container_set_border_width(GTK_CONTAINER(gm_win->mixer_frame), 6);
	gtk_box_pack_start(GTK_BOX(vbox), gm_win->mixer_frame, TRUE, TRUE, 0);
	gtk_widget_show(gm_win->mixer_frame);

	gm_win->mixer_container = gtk_mixer_container_create();
	gtk_container_add(GTK_CONTAINER(gm_win->mixer_frame),
	    gm_win->mixer_container);
	gtk_widget_show(gm_win->mixer_container);

	G_GNUC_BEGIN_IGNORE_DEPRECATIONS
	/* Single place still using deprecated API. GTK+ uses it internally as
	   well, so why should we suffer? Suffice it to say, new API is quite
	   limited. */
	bbox = gtk_dialog_get_action_area(GTK_DIALOG(gm_win->window));
	G_GNUC_END_IGNORE_DEPRECATIONS
	gtk_button_box_set_layout(GTK_BUTTON_BOX(bbox), GTK_BUTTONBOX_EDGE);
	gtk_container_set_border_width(GTK_CONTAINER(bbox), 6);

	gm_win->select_controls_button = gtk_button_new_with_mnemonic(
	    _("_Select Controls..."));
	gtk_button_set_image(GTK_BUTTON(gm_win->select_controls_button),
	    gtk_image_new_from_icon_name(
		"preferences-desktop", GTK_ICON_SIZE_BUTTON));
	gtk_actionable_set_action_name(
	    GTK_ACTIONABLE(gm_win->select_controls_button),
	    "app.select-controls");
	gtk_widget_set_sensitive(gm_win->select_controls_button, FALSE);
	gtk_box_pack_start(GTK_BOX(bbox), gm_win->select_controls_button,
	    FALSE, TRUE, 0);
	g_signal_connect(gm_win->select_controls_button, "clicked",
	    G_CALLBACK(gtk_mixer_window_action_select_controls), gm_win);
	gtk_widget_show(gm_win->select_controls_button);

	button = gtk_button_new_with_mnemonic(_("_Quit"));
	gtk_button_set_image(GTK_BUTTON(button),
	    gtk_image_new_from_icon_name("application-exit", GTK_ICON_SIZE_BUTTON));
	gtk_actionable_set_action_name(GTK_ACTIONABLE(button), "app.quit");
	gtk_widget_set_sensitive(button, TRUE);
	gtk_box_pack_start(GTK_BOX(bbox), button, FALSE, TRUE, 0);
	g_signal_connect(button, "clicked",
	    G_CALLBACK(gtk_mixer_window_quit), gm_win);
	gtk_widget_show(button);

	//g_signal_connect_swapped(G_OBJECT(gm_win->preferences),
	//    "notify::sound-card",
	//    G_CALLBACK(gtk_mixer_window_soundcard_property_changed), gm_win);

	//g_signal_connect_swapped(G_OBJECT(gm_win->preferences),
	//    "notify::controls",
	//    G_CALLBACK(gtk_mixer_window_controls_property_changed), gm_win);
	g_signal_connect(gm_win->window, "destroy",
	    G_CALLBACK(gtk_main_quit), gm_win);

	/* Update mixer controls for the active sound card */
	gtk_mixer_window_update_contents(gm_win);

	return (gm_win->window);
}

#if 0
static void
gtk_mixer_window_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
	XfceMixerWindow *window = XFCE_MIXER_WINDOW(widget);

	(*GTK_WIDGET_CLASS(gtk_mixer_window_parent_class)->size_allocate)(
	    widget, allocation);

	if (!gm_win->is_maximized && !gm_win->is_fullscreen)
		gtk_window_get_size(GTK_WINDOW(window), &gm_win->current_width,
		    &gm_win->current_height);
}


static gboolean
gtk_mixer_window_state_event(GtkWidget *widget, GdkEventWindowState *event)
{
	XfceMixerWindow *window = XFCE_MIXER_WINDOW(widget);
	gboolean result = GDK_EVENT_PROPAGATE;

	if (GTK_WIDGET_CLASS(gtk_mixer_window_parent_class)
		->window_state_event != NULL)
		result = (*GTK_WIDGET_CLASS(gtk_mixer_window_parent_class)
			       ->window_state_event)(widget, event);

	gm_win->is_maximized = (event->new_window_state &
				   GDK_WINDOW_STATE_MAXIMIZED) != 0;
	gm_win->is_fullscreen = (event->new_window_state &
				    GDK_WINDOW_STATE_FULLSCREEN) != 0;

	return result;
}



static void
gtk_mixer_window_soundcard_property_changed(XfceMixerWindow *window,
    GParamSpec *pspec,
    GObject *object)
{
	gchar *new_card_name;
	GstElement *new_card = NULL;
	GstElement *old_card;

	g_return_if_fail(IS_XFCE_MIXER_WINDOW(window));
	g_return_if_fail(G_IS_OBJECT(object));

	g_object_get(object, "sound-card", &new_card_name, NULL);
	if (new_card_name != NULL)
		new_card = gtk_mixer_get_card(new_card_name);
	g_free(new_card_name);

	/* If the new card is not valid reset it to the default */
	if (!GST_IS_MIXER(new_card)) {
		new_card = gtk_mixer_get_default_card();

		if (GST_IS_MIXER(new_card))
			g_object_set(object, "sound-card",
			    gtk_mixer_get_card_internal_name(new_card), NULL);

		return;
	}

	old_card = gtk_mixer_card_combo_get_active_card(
	    XFCE_MIXER_CARD_COMBO(gm_win->soundcard_combo));

	/* Only update if different from the active card */
	if (new_card != old_card) {
		/* Update the combobox */
		g_signal_handlers_block_by_func(
		    G_OBJECT(gm_win->soundcard_combo),
		    gtk_mixer_window_soundcard_changed, window);
		gtk_mixer_card_combo_set_active_card(
		    XFCE_MIXER_CARD_COMBO(gm_win->soundcard_combo), new_card);
		g_signal_handlers_unblock_by_func(
		    G_OBJECT(gm_win->soundcard_combo),
		    gtk_mixer_window_soundcard_changed, window);

		/* Update mixer controls for the active sound card */
		gtk_mixer_window_update_contents(window);

		/* Update the controls dialog */
		if (gm_win->controls_dialog != NULL)
			gtk_mixer_controls_dialog_set_soundcard(
			    XFCE_MIXER_CONTROLS_DIALOG(gm_win->controls_dialog),
			    new_card);
	}
}



static void
gtk_mixer_window_controls_property_changed(XfceMixerWindow *window,
    GParamSpec *pspec,
    GObject *object)
{
	gtk_mixer_container_update_contents(
	    XFCE_MIXER_CONTAINER(gm_win->mixer_container));

	/* Update the controls dialog */
	if (gm_win->controls_dialog != NULL)
		gtk_mixer_controls_dialog_update_dialog(
		    XFCE_MIXER_CONTROLS_DIALOG(gm_win->controls_dialog));
}



GstElement *
gtk_mixer_window_get_active_card(XfceMixerWindow *window)
{
	g_return_val_if_fail(IS_XFCE_MIXER_WINDOW(window), NULL);
	return gtk_mixer_card_combo_get_active_card(
	    XFCE_MIXER_CARD_COMBO(gm_win->soundcard_combo));
}



#endif
