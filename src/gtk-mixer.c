/*-
 * Copyright (c) 2020 - 2021 Rozhuk Ivan <rozhuk.im@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Rozhuk Ivan <rozhuk.im@gmail.com>
 *
 */


#include <sys/param.h>
#include <sys/types.h>
#include <err.h>
#include <errno.h>
#include <inttypes.h>
#include <stdio.h> /* snprintf, fprintf */
#include <time.h>
#include <string.h> /* bcopy, bzero, memcpy, memmove, memset, strerror... */
#include <stdlib.h> /* malloc, exit */
#include <unistd.h> /* close, write, sysconf */
#include <fcntl.h> /* open, fcntl */
#include <signal.h> /* SIGNAL constants. */

#define GETTEXT_PACKAGE "gtk-mixer"
#include <gtk/gtk.h>
#include <glib/gi18n-lib.h>


#include "gtk-mixer.h"


#define LOG_EV_FMT(fmt, args...)					\
	    fprintf(stderr, "%s , line: %i: " fmt "\n", __FUNCTION__, __LINE__, ##args)


#if 0
static void
activate(GtkApplication *app, gpointer user_data) {
	gm_app_p app = user_data;

	app->window = gtk_mixer_window_create(app);
	//gtk_widget_show_all(app.window);

	/* Display the mixer window */
	gtk_window_present(GTK_WINDOW(app->window));
}
#endif


int
main(int argc, char **argv) {
	int error = 0;
	gm_app_t app;

	memset(&app, 0x00, sizeof(gm_app_t));

	gm_plugin_init(&app.plugins, &app.plugins_count, &app.dev_list);

	for (size_t i = 0; i < app.dev_list.count; i ++) {
		LOG_EV_FMT("Plugin: %s: %s - %s",
		    app.dev_list.devs[i].plugin->descr->name,
		    app.dev_list.devs[i].name,
		    app.dev_list.devs[i].description);
	}


	/* Append application icons to the search path. */
	//gtk_icon_theme_append_search_path(gtk_icon_theme_get_default(),
	//    GTK_MIXER_DATADIR G_DIR_SEPARATOR_S "icons");

	/* Set application name. */
	g_set_application_name(_("Audio Mixer"));

	/* Use volume control icon for all mixer windows. */
	gtk_window_set_default_icon_name("multimedia-volume-control");

	gtk_init(&argc, &argv);

	app.window = gtk_mixer_window_create(&app);

	/* Display the mixer window. */
	//gtk_widget_show_all(app.window);
	gtk_window_present(GTK_WINDOW(app.window));

	gtk_main();


	return (error);

	/*GtkApplication *app = gtk_application_new("org.gtk-mixer", G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
	error = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);

	return (error);*/
}

const char *
volume_stock_from_level(const int is_mic, const int is_enabled,
    const int level, const char *cur_icon_name) {
	const int levels[] = { -1, 0, 33, 66, 100 };
	const char *volume_level[nitems(levels)] = {
	    "audio-volume-muted",
	    "audio-volume-muted",
	    "audio-volume-low",
	    "audio-volume-medium",
	    "audio-volume-high"
	};
	const char *mic_sens_level[nitems(levels)] = {
	    "microphone-disabled-symbolic",
	    "microphone-sensitivity-muted",
	    "microphone-sensitivity-low",
	    "microphone-sensitivity-medium",
	    "microphone-sensitivity-high"
	};
	const char **stocks = ((0 != is_mic) ? mic_sens_level : volume_level);

	for (size_t i = 0; i < nitems(levels); i ++) {
		if (levels[i] < level && 0 != is_enabled)
			continue;
		if (NULL != cur_icon_name &&
		    strcmp(cur_icon_name, stocks[i]) == 0)
			break; /* No need to update. */
		return (stocks[i]);
	}

	return (NULL);
}
