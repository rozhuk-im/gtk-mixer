/*-
 * Copyright (c) 2020 - 2021 Rozhuk Ivan <rozhuk.im@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Rozhuk Ivan <rozhuk.im@gmail.com>
 *
 */


#include <sys/param.h>
#include <sys/types.h>
#if defined(__DragonFly__) || defined(__FreeBSD__)
#include <sys/sysctl.h>
#endif
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <sys/stat.h>

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "plugin_api.h"


#define PATH_DEV_MIXER "/dev/mixer"

static const char *oss_line_labels[] = SOUND_DEVICE_LABELS;

enum {
	MIXER_STATE_RECSRC = 0,
	MIXER_STATE_DEVMASK,
	MIXER_STATE_RECMASK,
	MIXER_STATE_CAPS,
	MIXER_STATE_STEREODEVS
};

static const uint32_t mixer_state_id[] = {
	SOUND_MIXER_RECSRC, /* 1 bit per recording source */
	SOUND_MIXER_DEVMASK, /* 1 bit per supported device */
	SOUND_MIXER_RECMASK, /* 1 bit per supp. recording source */
	SOUND_MIXER_CAPS, /* Flags: SOUND_CAP_EXCL_INPUT - Only 1 rec. src at a time */
	SOUND_MIXER_STEREODEVS, /* Mixer channels supporting stereo */
};


typedef struct gtk_mixer_plugin_device_oss_state_s {
	int state[nitems(mixer_state_id)];
} gmp_dev_oss_state_t, *gmp_dev_oss_state_p;



static void
device_descr_get(size_t dev_idx, char *buf, size_t buf_size) {
#if defined(__DragonFly__) || defined(__FreeBSD__)
	char dev_path[32];
	size_t tmp = (buf_size - 1);

	snprintf(dev_path, sizeof(dev_path), "dev.pcm.%zu.%%desc", dev_idx);
	if (sysctlbyname(dev_path, buf, &tmp, NULL, 0) != 0) {
		tmp = 0;
	}
	buf[tmp] = 0x00;
#elif defined(SOUND_MIXER_INFO)
	size_t tmp = 0;
	char dev_path[32];
	mixer_info mi;

	snprintf(dev_path, sizeof(dev_path), PATH_DEV_MIXER"%zu", dev_idx);
	int fd = open(dev_path, O_RDONLY);
	if (ioctl(fd, SOUND_MIXER_INFO, &mi) == 0) {
		strncpy(buf, mi.name, buf_size);
		tmp = buf_size;
	}
	close(fd);
	buf[tmp] = 0x00;
#else
	buf[0] = 0x00;
#endif
}

static void
oss_list_devs(gm_plugin_p plugin, gmp_dev_list_p dev_list) {
	struct stat st;
	char dev_path[32] = PATH_DEV_MIXER, dev_descr[256] = "Default";
	gmp_dev_t dev = { .name = dev_path, .description = dev_descr };

	if (stat(PATH_DEV_MIXER, &st) == 0) {
		gm_plugin_dev_list_add(plugin, dev_list, &dev);
	}

	/* Auto detect. */
	for (size_t i = 0, fail_cnt = 0; fail_cnt < 8; i ++, fail_cnt ++) {
		snprintf(dev_path, sizeof(dev_path), PATH_DEV_MIXER"%zu", i);
		if (stat(dev_path, &st) != 0)
			continue;
		device_descr_get(i, dev_descr, sizeof(dev_descr));
		gm_plugin_dev_list_add(plugin, dev_list, &dev);
		fail_cnt = 0; /* Reset fail counter. */
	}
}


static int
oss_dev_init(gmp_dev_p dev) {
	int error, fd, chan_mask;
	gmp_dev_oss_state_p oss_state;
	gmp_dev_line_p dev_line;

	if (NULL == dev)
		return (EINVAL);

	oss_state = calloc(1, sizeof(gmp_dev_oss_state_t));
	if (NULL == oss_state)
		return (ENOMEM);

	/* State, caps, settings. */
	fd = open(dev->name, O_RDONLY);
	if (-1 == fd) {
		error = errno;
		goto err_out;
	}
	for (size_t i = 0; i < nitems(mixer_state_id); i ++) {
		if (-1 == ioctl(fd, MIXER_READ(mixer_state_id[i]),
		    &oss_state->state[i])) {
			oss_state->state[i] = 0;
		}
	}
	close(fd);

	/* Lines / channels add. */
	for (size_t i = 0; i < SOUND_MIXER_NRDEVICES; i ++) {
		chan_mask = (((int)1) << i);
		if (0 == (chan_mask & oss_state->state[MIXER_STATE_DEVMASK]))
			continue;
		/* Add line. */
		error = gm_plugin_dev_line_add(dev, oss_line_labels[i], &dev_line);
		if (0 != error)
			goto err_out;
		dev_line->priv = (void*)i;
		dev_line->chan_map = (((uint32_t)1) << MIXER_CHANNEL_FL);
		dev_line->chan_vol_count = 1;
		if (0 != (chan_mask & oss_state->state[MIXER_STATE_STEREODEVS])) {
			dev_line->chan_map |= (((uint32_t)1) << MIXER_CHANNEL_FR);
			dev_line->chan_vol_count ++;
		}
		dev_line->is_capture = (0 != (chan_mask & oss_state->state[MIXER_STATE_RECMASK]));
		dev_line->is_read_only = 0;
		/* All rec lines can be disabled. */
		dev_line->has_enable = dev_line->is_capture;
	}

	dev->priv = oss_state;

	return (0);

err_out:
	free(oss_state);

	return (error);
}

static void
oss_dev_uninit(gmp_dev_p dev) {

	if (NULL == dev)
		return;

	free(dev->priv);
	dev->priv = NULL;
}

static int
oss_dev_line_read(gmp_dev_p dev, gmp_dev_line_p dev_line,
    gmp_dev_line_state_p state) {
	int error = 0, fd, chan_mask, vol;
	size_t chan_idx;
	gmp_dev_oss_state_p oss_state;

	if (NULL == dev || NULL == dev_line || NULL == state)
		return (EINVAL);

	oss_state = dev->priv;
	chan_idx = ((size_t)dev_line->priv);
	chan_mask = (((int)1) << chan_idx);
	if (0 == (chan_mask & oss_state->state[MIXER_STATE_DEVMASK]))
		return (EINVAL);

	/* Read state. */
	fd = open(dev->name, O_RDONLY);
	if (-1 == fd)
		return (errno);
	/* Volume level. */
	if (-1 == ioctl(fd, MIXER_READ(chan_idx), &vol)) {
		error = errno;
		goto err_out;
	}
	/* Map OSS to app values. */
	/* Left channel. */
	state->chan_vol[MIXER_CHANNEL_FL] = (vol & 0x7f);
	/* Right channel. */
	if (0 != (chan_mask & oss_state->state[MIXER_STATE_STEREODEVS])) {
		state->chan_vol[MIXER_CHANNEL_FR] = ((vol >> 8) & 0x7f);
	}
	/* Enabled state. */
	if (dev_line->is_capture) {
		if (-1 == ioctl(fd, MIXER_READ(mixer_state_id[MIXER_STATE_RECSRC]),
		    &oss_state->state[MIXER_STATE_RECSRC])) {
			error = errno;
			goto err_out;
		}
		/* Map OSS to app values. */
		state->is_enabled = (0 != (chan_mask &
		    oss_state->state[MIXER_STATE_RECSRC]));
	}

err_out:
	close(fd);

	return (error);
}

static int
oss_dev_line_write(gmp_dev_p dev, gmp_dev_line_p dev_line,
    gmp_dev_line_state_p state) {
	int error = 0, fd, chan_mask, vol;
	size_t chan_idx;
	gmp_dev_oss_state_p oss_state;

	if (NULL == dev || NULL == dev_line || NULL == state)
		return (EINVAL);

	oss_state = dev->priv;
	chan_idx = ((size_t)dev_line->priv);
	chan_mask = (((int)1) << chan_idx);
	if (0 == (chan_mask & oss_state->state[MIXER_STATE_DEVMASK]))
		return (EINVAL);

	/* Map app to OSS values. */
	vol = state->chan_vol[MIXER_CHANNEL_FL];
	if (0 != (chan_mask & oss_state->state[MIXER_STATE_STEREODEVS])) {
		vol |= (state->chan_vol[MIXER_CHANNEL_FR] << 8);
	}
	/* Write state. */
	fd = open(dev->name, O_RDWR);
	if (-1 == fd)
		return (errno);
	/* Volume level. */
	if (-1 == ioctl(fd, MIXER_WRITE(chan_idx), &vol)) {
		error = errno;
		goto err_out;
	}
	/* Enabled state. */
	if (dev_line->is_capture) {
		/* Map OSS to app values. */
		if (0 != state->is_enabled) {
			oss_state->state[MIXER_STATE_RECSRC] |= chan_mask;
		} else {
			oss_state->state[MIXER_STATE_RECSRC] &= ~chan_mask;
		}
		if (-1 == ioctl(fd, MIXER_WRITE(mixer_state_id[MIXER_STATE_RECSRC]),
		    &oss_state->state[MIXER_STATE_RECSRC])) {
			error = errno;
			goto err_out;
		}
		/* Read to ensure that it got aplly. */
		if (-1 == ioctl(fd, MIXER_READ(mixer_state_id[MIXER_STATE_RECSRC]),
		    &oss_state->state[MIXER_STATE_RECSRC])) {
			error = errno;
			goto err_out;
		}
		/* Map OSS to app values. */
		state->is_enabled = (0 != (chan_mask &
		    oss_state->state[MIXER_STATE_RECSRC]));
	}

err_out:
	close(fd);

	return (error);
}

const gmp_descr_t plugin_oss3 = {
	.name		= "OSS",
	.description	= "OSSv3 Mixer driver plugin",
	.plugin_init	= NULL,
	.plugin_uninit	= NULL,
	.plugin_list_devs= oss_list_devs,
	.dev_init	= oss_dev_init,
	.dev_uninit	= oss_dev_uninit,
	.dev_line_read	= oss_dev_line_read,
	.dev_line_write	= oss_dev_line_write,
};
