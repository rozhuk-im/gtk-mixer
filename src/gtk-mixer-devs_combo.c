/*-
 * Copyright (c) 2008 Jannis Pohlmann <jannis@xfce.org>
 * Copyright (c) 2020 - 2021 Rozhuk Ivan <rozhuk.im@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */


#include <sys/param.h>
#include <sys/types.h>
#include <inttypes.h>

#include "gtk-mixer.h"

#define GM_CMB_DEVS_COLUMN_NAME	0
#define GM_CMB_DEVS_COLUMN_CARD	1


static void
gtk_mixer_devs_combo_changed(GtkWidget *combo, gpointer user_data) {
	gm_app_p app = user_data;
	gmp_dev_p dev_new = gtk_mixer_devs_combo_get_active_device(combo);
	gmp_dev_p dev_old = app->dev_current;

	gm_plugin_dev_init(dev_new);
	app->dev_current = dev_new;
	gm_plugin_dev_uninit(dev_old);
}

static void
gtk_mixer_devs_combo_destroy(GtkWidget *combo __unused, gpointer user_data) {
	GtkListStore *list_store = user_data;

	gtk_list_store_clear(list_store);
	g_object_unref(list_store);
}

GtkWidget *
gtk_mixer_devs_combo_create(gm_app_p app, gmp_dev_p dev) {
	GtkWidget *combo;
	GtkListStore *list_store;
	GtkCellRenderer *renderer;
	GtkTreeIter tree_iter;
	char display_name[256];

	combo = gtk_combo_box_new();

	list_store = gtk_list_store_new(2, G_TYPE_STRING,
	    G_TYPE_POINTER);
	g_object_set_data(G_OBJECT(combo), "list_store", list_store);
	gtk_combo_box_set_model(GTK_COMBO_BOX(combo),
	    GTK_TREE_MODEL(list_store));

	renderer = gtk_cell_renderer_text_new();
	g_object_set(G_OBJECT(renderer), "ellipsize",
	    PANGO_ELLIPSIZE_END, NULL);
	gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(combo), renderer, TRUE);
	gtk_cell_layout_add_attribute(GTK_CELL_LAYOUT(combo), renderer,
	    "text", GM_CMB_DEVS_COLUMN_NAME);

	for (size_t i = 0; i < app->dev_list.count; i ++) {
		snprintf(display_name, sizeof(display_name),
		    "%s: %s (%s)",
		    app->dev_list.devs[i].plugin->descr->name,
		    app->dev_list.devs[i].description,
		    app->dev_list.devs[i].name);
		gtk_list_store_append(list_store, &tree_iter);
		gtk_list_store_set(list_store, &tree_iter,
		    GM_CMB_DEVS_COLUMN_NAME, display_name,
		    GM_CMB_DEVS_COLUMN_CARD, &app->dev_list.devs[i], -1);
	}

	g_signal_connect(combo, "changed",
	    G_CALLBACK(gtk_mixer_devs_combo_changed), app);
	g_signal_connect(combo, "destroy",
	    G_CALLBACK(gtk_mixer_devs_combo_destroy), list_store);

	gtk_mixer_devs_combo_set_active_device(combo, dev);

	return (combo);
}

gmp_dev_p
gtk_mixer_devs_combo_get_active_device(GtkWidget *combo) {
	gmp_dev_p dev = NULL;
	GtkTreeIter iter;
	GtkListStore *list_store = g_object_get_data(G_OBJECT(combo), "list_store");

	if (gtk_combo_box_get_active_iter(GTK_COMBO_BOX(combo), &iter)) {
		gtk_tree_model_get(GTK_TREE_MODEL(list_store), &iter,
		    GM_CMB_DEVS_COLUMN_CARD, &dev, -1);
	}

	return (dev);
}

void
gtk_mixer_devs_combo_set_active_device(GtkWidget *combo,
    gmp_dev_p dev) {
	gmp_dev_p device_cur = NULL;
	GtkTreeIter iter;
	gboolean valid_iter;
	GtkListStore *list_store = g_object_get_data(G_OBJECT(combo), "list_store");

	if (NULL == dev) {
		gtk_combo_box_set_active(GTK_COMBO_BOX(combo), 0);
		return;
	}
	valid_iter = gtk_tree_model_get_iter_first(
	    GTK_TREE_MODEL(list_store), &iter);
	while (valid_iter) {
		gtk_tree_model_get(GTK_TREE_MODEL(list_store),
		    &iter, GM_CMB_DEVS_COLUMN_CARD, &device_cur, -1);
		if (device_cur == dev)
			break;
		valid_iter = gtk_tree_model_iter_next(
		    GTK_TREE_MODEL(list_store), &iter);
	}
	gtk_combo_box_set_active_iter(GTK_COMBO_BOX(combo), &iter);
}
